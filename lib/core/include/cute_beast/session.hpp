#ifndef CUTE_BEAST_SESSION_HPP
#define CUTE_BEAST_SESSION_HPP

#include "impl/logging.hpp"

#include "impl/detail.hpp"
#include "impl/http_generator.hpp"
#include <boost/asio/async_result.hpp>
#include <boost/asio/compose.hpp>
#include <boost/asio/coroutine.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/execution/allocator.hpp>
#include <boost/asio/execution/context.hpp>
#include <boost/asio/execution/relationship.hpp>
#include <boost/asio/execution_context.hpp>
#include <boost/asio/ip/network_v4.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/core/buffer_traits.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/callable_traits.hpp>
#include <boost/noncopyable.hpp>
#include <boost/url/url_view.hpp>
#include <charconv>
#include <cstddef>
#include <iostream>
#include <mutex>
#include <optional>
#include <regex>
#include <shared_mutex>
#include <tuple>
#include <type_traits>
#include <utility>

namespace cute_beast
{

#define CUTE_BEAST_VERSION_STRING "CuteBeast/" BOOST_BEAST_VERSION_STRING

enum class session_mode {
    http,
    https,
    both_http_https,
};

namespace session
{
    constexpr size_t start_buffer_size = 1024 * 4;
    class websocket;
    class http;
} // namespace session

using match_result = std::optional<std::cmatch>;

struct basic_http_route_rule {
protected:
    std::regex pattern_;
    /**
     * allowed request methods
     */
    const std::uint64_t methods_;
    CUTE_BEAST_LOGGER_T logger_ = CUTE_BEAST_MAKE_LOGGER("uri_route_rule_{}", (void*)this);

public:
    [[nodiscard]] CUTE_BEAST_LOGGER_T logger() const
    {
        return logger_;
    }
    [[nodiscard]] std::uint64_t methods() const
    {
        return methods_;
    }
    explicit basic_http_route_rule(std::string_view pattern,
        std::uint64_t methods = detail::make_methods_mask({
            http::verb::get,
            http::verb::head,
        }))
        : pattern_(pattern.data())
        , methods_(methods)
    {
    }
    virtual ~basic_http_route_rule() = default;

    /**
     * @brief impl must morph parser into required body type
     * @param session
     */
    virtual void on_header_done(session::http& session) const = 0;
    /**
     *
     * @param session
     * @return
     */
    [[nodiscard]] virtual match_result is_matching(session::http& session) const = 0;

    virtual void handle_request(session::http&& session) const = 0;

    /**
     * check if target matching pattern
     * @param target
     * @return
     */
    [[nodiscard]] match_result is_matching(string_view target) const
    {
        CUTE_BEAST_LOG_TRACE(logger_, "{}({})", __func__, std::string_view(target.data(), target.size()));
        std::cmatch match;

        auto matching = std::regex_match(target.begin(), target.end(), match, pattern_);
        if (!matching) {
            return std::nullopt;
        }
        return match;
    };
};

using route_ptr = std::shared_ptr<basic_http_route_rule>;

namespace args
{
    /**
     * just an alias for tuple to store handler arguments
     */
    template <class... Args> using pack = std::tuple<Args...>;
    using empty = pack<>;
} // namespace args

class router
{
    mutable std::shared_mutex http_mutex_;
    mutable std::shared_mutex websocket_mutex_;
    std::vector<route_ptr> http_rules_;
    std::unordered_map<std::string, std::function<void(std::shared_ptr<session::websocket>)>> ws_rules_;
    CUTE_BEAST_LOGGER_T logger_ = CUTE_BEAST_MAKE_LOGGER("router_{}", (void*)this);

public:
    explicit router(std::vector<route_ptr> rules = {});
    void add_rule(route_ptr ptr);
    template <class ArgsT = args::empty, class RequestBodyT = http::string_body, class Handler>
    void add_rule(std::string_view pattern, Handler&& handler,
        std::initializer_list<http::verb>&& methods = {
            http::verb::get,
            http::verb::head,
        });
    size_t remove_matched(string_view pattern);
    void add_websocket_rule(std::string path, std::function<void(std::shared_ptr<session::websocket>)> cb);
    size_t remove_websocket_matched(std::string pattern);
    [[nodiscard]] const basic_http_route_rule* match(session::http& session) const;
    [[nodiscard]] std::function<void(std::shared_ptr<session::websocket>)> match(const session::websocket& session) const;
};

//----------------------------------------------------------

template <class ArgsT = args::empty, class RequestBodyT = http::string_body, class Handler>
/**
 * @brief main function to create route rule
 * @tparam ArgsT tuple of arguments that will be passed to handler from third
 * @tparam RequestBodyT controls http parser algorithm
 * @tparam Handler request handler, first two arguments are session, request
 * @param pattern string representation of regular expression for rule to match
 * @param handler @see Handler
 * @param methods acceptable methods
 * @return unique_ptr for rule
 */
route_ptr make_route_rule(std::string_view pattern, Handler&& handler,
    std::initializer_list<http::verb>&& methods = {
        http::verb::get,
        http::verb::head,
    });

namespace util
{
    void send(session::http&& session, http_generator&& gen);

    template <bool isRequest, class Body, class Fields> void send(session::http&& session, http::message<isRequest, Body, Fields>&& msg)
    {
        send(std::move(session), http_generator(std::move(msg)));
    }

    template <class Body, class Request> auto make_response(Request&& req, http::status status, typename Body::value_type&& value = {})
    {
        http::response<Body> res { status, req.version() };
        res.set(http::field::server, CUTE_BEAST_VERSION_STRING);
        res.keep_alive(req.keep_alive());
        if (req.method() != http::verb::head) {
            // we must not send body if request for head
            res.body() = std::move(value);
        }
        res.prepare_payload();
        return res;
    }
    template <class Request> auto make_response(Request&& req, http::status status, string_view text)
    {
        return make_response<http::string_body>(req, status, std::string(text));
    }
    template <class Request> auto make_bad_request(Request&& req, string_view why)
    {
        return make_response(req, http::status::bad_request, why);
    }
    template <class Request> auto make_internal_server_error(Request&& req, string_view why = {})
    {
        return make_response(req, http::status::internal_server_error, why);
    }
    template <class Request> auto make_not_found(Request&& req)
    {
        return make_response(req, http::status::not_found, req.target());
    }
    template <class Request> auto make_ok(Request&& req, string_view target)
    {
        return make_response(req, http::status::ok, target);
    }

    template <bool isRequest, class Body, class Fields> http_generator make_gen(http::message<isRequest, Body, Fields>&& m)
    {
        return http_generator(std::move(m));
    }
} // namespace util

namespace session
{

    class websocket : public std::enable_shared_from_this<websocket>
    {

    public:
        using error = beast::websocket::error;
        using dynamic_string_buffer
            = net::dynamic_string_buffer<std::string::value_type, std::string::traits_type, std::string::allocator_type>;

        void async_read(dynamic_string_buffer& buf, std::function<void(error_code, std::size_t, bool)> on_read)
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);
            impl_->async_read(buf, std::move(on_read));
        }

        void async_write(net::const_buffer buf, bool is_text, std::function<void(error_code, std::size_t)> on_write)
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);
            impl_->async_write(buf, is_text, std::move(on_write));
        }
        bool is_secure() const
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

            return impl_->is_secure();
        };

    private:
        // Start the asynchronous operation
        void do_accept(const beast::http::request<beast::http::string_body>& req)
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

            impl_->do_accept(req);
        }

        void on_accept(error_code ec)
        {
            if (ec) {
                CUTE_BEAST_LOG_ERROR(logger_, "{}: {}", __func__, ec.message());
                return;
            }

            // pass self to handler
            auto matched = router_.match(*this);
            if (matched) {
                CUTE_BEAST_LOG_TRACE(logger_, "{}: {}", __func__, "found matching path, passing self to handler");
                matched(shared_from_this());
                return;
            }
            CUTE_BEAST_LOG_WARN(logger_, "{}: {}", __func__, "matching rule not found");

            //TODO: decide wwhther to give ws control to handler before accept
            //TODO: log failed handler
        }

    private:
        struct websocket_impl;
        template <class StreamT> static std::unique_ptr<websocket_impl> make_websocket_impl(websocket& ws, StreamT stream)
        {
            struct stream_t_websocket_impl : public websocket_impl {
                beast::websocket::stream<StreamT> stream_;
                stream_t_websocket_impl(websocket& ws, StreamT&& stream)
                    : websocket_impl(ws)
                    , stream_(std::move(stream))
                {
                }
                bool is_secure() override
                {
                    return std::is_same_v<std::decay_t<StreamT>, beast::ssl_stream<beast::tcp_stream>>;
                }

                void do_accept(const beast::http::request<boost::beast::http::string_body>& req) override
                { // Set suggested timeout settings for the websocket
                    stream_.set_option(beast::websocket::stream_base::timeout::suggested(beast::role_type::server));

                    // Set a decorator to change the Server of the handshake
                    stream_.set_option(beast::websocket::stream_base::decorator(
                        [](beast::websocket::response_type& res) { res.set(beast::http::field::server, CUTE_BEAST_VERSION_STRING); }));

                    // Accept the websocket handshake
                    stream_.async_accept(req, beast::bind_front_handler(&websocket::on_accept, self_.shared_from_this()));
                }

                void async_read(dynamic_string_buffer& buf, std::function<void(error_code, std::size_t, bool)> on_read) override
                {
                    stream_.async_read(
                        buf, [this, self = self_.shared_from_this(), on_read = std::move(on_read)](error_code ec, std::size_t bytes_read) {
                            on_read(ec, bytes_read, stream_.text());
                        });
                }
                void async_write(net::const_buffer buf, bool is_text, std::function<void(error_code, std::size_t)> on_write) override
                {
                    stream_.text(is_text);
                    stream_.async_write(buf, [self = self_.shared_from_this(), on_write](error_code ec, std::size_t bytes_transferred) {
                        on_write(ec, bytes_transferred);
                    });
                }
            };
            return std::make_unique<stream_t_websocket_impl>(ws, std::move(stream));
        }
        std::optional<beast::http::request<beast::http::string_body>> req_;
        const router& router_;
        CUTE_BEAST_LOGGER_T logger_;

    public:
        template <class StreamT>
        explicit websocket(StreamT stream, const router& router)
            : impl_(make_websocket_impl(*this, std::move(stream)))
            , router_(router)
            , logger_(this->is_secure() ? CUTE_BEAST_MAKE_LOGGER("wss_session_{}", (void*)this)
                                        : CUTE_BEAST_MAKE_LOGGER("ws_session_{}", (void*)this))
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);
        }
        // Start the asynchronous operation
        void run(beast::http::request<beast::http::string_body> req)
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

            req_.emplace(std::move(req));
            // Accept the WebSocket upgrade request
            do_accept(*req_);
        }

    private:
        struct websocket_impl {

        protected:
            websocket& self_;
            explicit websocket_impl(websocket& self)
                : self_(self)
            {
            }

        public:
            virtual ~websocket_impl() = default;
            virtual void do_accept(const beast::http::request<beast::http::string_body>& req) = 0;
            virtual void async_read(dynamic_string_buffer& buf, std::function<void(error_code, std::size_t, bool)> on_read) = 0;
            virtual void async_write(net::const_buffer buf, bool is_text, std::function<void(error_code, std::size_t)> on_write) = 0;
            virtual bool is_secure() = 0;
        };
        std::unique_ptr<websocket_impl> impl_;

        friend class ::cute_beast::router;
    };

    //------------------------------------------------------------------------------

    class http : public std::enable_shared_from_this<http>
    {
        template <class Session> friend auto& detail::session_match_result(Session& session);
        template <class Session, class BodyT> friend auto detail::release_request(Session& session);
        template <class Session, class BodyT> friend const auto& detail::get_request(Session& session);
        template <class BodyT, class FromBodyT, class Session> friend void detail::morph_parser(Session& session);
        template <class BodyT, class Session> friend auto detail::parser_ptr(Session& session);
        template <class Session> friend void detail::session_set_url(Session& session, url_view&& url);

        friend class ::cute_beast::router;

    private:
        url_view url_;
        std::unique_ptr<beast::http::basic_parser<true>> parser_;
        //current match_result
        match_result match_result_;
        //current mahcing rule
        const basic_http_route_rule* rr_ = nullptr;

        beast::flat_buffer buffer_;
        const router& router_;
        struct session_impl {
        protected:
            http& self_;
            explicit session_impl(http& self)
                : self_(self)
            {
            }

        public:
            virtual ~session_impl() = default;
            virtual void run() = 0;
            virtual void async_read_header() = 0;
            virtual void async_read_body() = 0;
            virtual bool is_secure() = 0;
            virtual void do_eof() = 0;
            virtual void continue_as_websocket() = 0;
            virtual void send(http_generator&& gen) = 0;
        };

        class ssl_session_impl : public session_impl
        {
            std::shared_ptr<ssl::context> ssl_ctx_;
            beast::ssl_stream<beast::tcp_stream> stream_;

        public:
            ssl_session_impl(http& self, tcp::socket&& socket, std::shared_ptr<ssl::context> ssl_ctx)
                : session_impl(self)
                , ssl_ctx_(std::move(ssl_ctx))
                , stream_(std::move(socket), *ssl_ctx_)
            {
            }
            void run() override
            {
                // We need to be executing within a strand t o perform async operations
                // on the I/O objects in this session.
                net::dispatch(stream_.get_executor(), [this, self = self_.shared_from_this()]() mutable {
                    beast::get_lowest_layer(stream_).expires_after(std::chrono::seconds(30));
                    // Perform the SSL handshake
                    // Note, this is the buffered version of the handshake.
                    stream_.async_handshake(net::ssl::stream_base::server, self_.buffer_.data(),
                        [this, selfz = std::move(self)](error_code ec, std::size_t bytes_used) { on_handshake(ec, bytes_used); });
                });
            }

            void on_handshake(error_code ec, std::size_t bytes_used)
            {
                if (ec) {
                    CUTE_BEAST_LOG_ERROR(self_.logger_, "{}: {}", __func__, ec.message());
                    return;
                }

                // Consume the portion of the buffer used by the handshake
                self_.buffer_.consume(bytes_used);

                self_.do_read_header();
            }
            void async_read_header() override
            {
                // Set the timeout. That's timeout per REQUEST from reading header to writing the answer, it would be renewed here if several requests happend in single session
                //TODO think how to make it configurable
                beast::get_lowest_layer(stream_).expires_after(std::chrono::seconds(30));
                beast::http::async_read_header(
                    stream_, self_.buffer_, *self_.parser_, beast::bind_front_handler(&http::on_read_header, self_.shared_from_this()));
            }
            void async_read_body() override
            {
                beast::http::async_read(
                    stream_, self_.buffer_, *self_.parser_, beast::bind_front_handler(&http::on_read_body, self_.shared_from_this()));
            }
            bool is_secure() override
            {
                return true;
            }
            void do_eof() override
            {
                // Set the timeout.
                beast::get_lowest_layer(stream_).expires_after(std::chrono::seconds(30));

                // Perform the SSL shutdown
                stream_.async_shutdown([this, self = self_.shared_from_this()](error_code ec) {
                    if (ec) {
                        CUTE_BEAST_LOG_ERROR(self_.logger_, "{}: {}", "async_shutdown", ec.message());
                        return;
                    }
                });
            }
            void continue_as_websocket() override
            {
                auto real_parser = static_cast<beast::http::request_parser<beast::http::string_body>*>(self_.parser_.get());
                // Disable the timeout.
                // The websocket::stream uses its own timeout settings.
                beast::get_lowest_layer(stream_).expires_never();
                std::make_shared<websocket>(std::move(stream_), self_.router_)->run(real_parser->release());
            }
            void send(http_generator&& gen) override
            {
                auto need_eof = gen.need_eof();
                async_write(stream_, std::move(gen), beast::bind_front_handler(&http::on_write, self_.shared_from_this(), need_eof));
            }
        };
        class http_session_impl : public session_impl
        {
            beast::tcp_stream stream_;

        public:
            http_session_impl(http& self, tcp::socket&& socket)
                : session_impl(self)
                , stream_(std::move(socket))
            {
            }
            void run() override
            {
                net::dispatch(stream_.get_executor(), beast::bind_front_handler(&http::do_read_header, self_.shared_from_this()));
            }
            void async_read_header() override
            {
                // Set the timeout. That's timeout per REQUEST from reading header to writing the answer, it would be renewed here if several requests happend in single session
                //TODO think how to make it configurable
                beast::get_lowest_layer(stream_).expires_after(std::chrono::seconds(30));
                beast::http::async_read_header(
                    stream_, self_.buffer_, *self_.parser_, beast::bind_front_handler(&http::on_read_header, self_.shared_from_this()));
            }
            void async_read_body() override
            {
                beast::http::async_read(
                    stream_, self_.buffer_, *self_.parser_, beast::bind_front_handler(&http::on_read_body, self_.shared_from_this()));
            }
            bool is_secure() override
            {
                return false;
            }
            void do_eof() override
            {
                error_code _;
                stream_.socket().shutdown(tcp::socket::shutdown_send, _);
            }
            void continue_as_websocket() override
            {
                auto real_parser = static_cast<beast::http::request_parser<beast::http::string_body>*>(self_.parser_.get());
                // Disable the timeout.
                // The websocket::stream uses its own timeout settings.
                beast::get_lowest_layer(stream_).expires_never();
                std::make_shared<websocket>(std::move(stream_), self_.router_)->run(real_parser->release());
            }
            void send(http_generator&& gen) override
            {

                auto eof = gen.need_eof();
                //                beast::bind_front_handler(&ssl_detector::on_detect, shared_from_this()));
                async_write(stream_, std::move(gen), beast::bind_front_handler(&http::on_write, self_.shared_from_this(), eof));
            }
        };
        std::unique_ptr<session_impl> impl_ = {};
        CUTE_BEAST_LOGGER_T logger_;

        static std::unique_ptr<session_impl> make_impl(http& session, tcp::socket&& socket_, std::shared_ptr<ssl::context> ssl_ctx = {})
        {
            if (ssl_ctx) {
                return std::make_unique<ssl_session_impl>(session, std::move(socket_), std::move(ssl_ctx));
            }
            return std::make_unique<http_session_impl>(session, std::move(socket_));
        }

    public:
        // Take ownership of the buffer
        http(tcp::socket&& socket, beast::flat_buffer&& buffer, const router& router, std::shared_ptr<ssl::context> ssl_ctx = {})
            : buffer_(std::move(buffer))
            , router_(router)
            , impl_(make_impl(*this, std::move(socket), std::move(ssl_ctx)))
            , logger_(this->is_secure() ? CUTE_BEAST_MAKE_LOGGER("https_session_{}", (void*)this)
                                        : CUTE_BEAST_MAKE_LOGGER("http_session_{}", (void*)this))
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);
        }
        ~http()
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);
        }
        void run()
        {
            impl_->run();
        }

        bool is_secure() const
        {
            return impl_->is_secure();
        }

    private:
        void do_read_header()
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

            // Construct a new parser for each message
            parser_ = std::make_unique<beast::http::request_parser<beast::http::string_body>>();

            // Apply a reasonable limit to the allowed size
            // of the body in bytes to prevent abuse.

            parser_->body_limit(boost::none);

            // Read a request
            impl_->async_read_header();
        }
        void do_read_body()
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

            // Read a request
            impl_->async_read_body();
        }
        void on_read_body(error_code ec, std::size_t bytes_transferred)
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

            boost::ignore_unused(bytes_transferred);

            // This means they closed the connection
            if (ec == beast::http::error::end_of_stream) {
                impl_->do_eof();
                return;
            }

            if (ec) {
                CUTE_BEAST_LOG_ERROR(logger_, "{}: {}", __func__, ec.message());
                return;
            }

            rr_->handle_request(std::move(*this));
        }
        void on_read_header(error_code ec, std::size_t bytes_transferred)
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

            boost::ignore_unused(bytes_transferred);

            // This means they closed the connection
            if (ec == beast::http::error::end_of_stream) {
                CUTE_BEAST_LOG_TRACE(logger_, "{}() {}", __func__, ec.message());

                return impl_->do_eof();
            }

            if (ec) {
                CUTE_BEAST_LOG_ERROR(logger_, "{}: {}", __func__, ec.message());
                return;
            }
            //here our parser always of 'default' string body so cast it and ask if we need to upgrade to ws
            auto real_parser = static_cast<beast::http::request_parser<beast::http::string_body>*>(parser_.get());
            // See if it is a WebSocket Upgrade
            if (beast::websocket::is_upgrade(real_parser->get())) {
                CUTE_BEAST_LOG_DEBUG(logger_, "-- upgrade to websocket -->");

                // Create a websocket session, transferring ownership
                // of both the socket and the HTTP request.
                return impl_->continue_as_websocket();
            }
            auto rule = router_.match(*this);
            if (rule) {
                CUTE_BEAST_LOG_TRACE(logger_, "found matching rule");

                this->rr_ = rule;
                rule->on_header_done(*this);
                do_read_body();
                return;
            }
            CUTE_BEAST_LOG_TRACE(logger_, "route rule not found, send 404");

            //TODO: check if we can respond with 'not found' before we got body?
            impl_->send(util::make_not_found(real_parser->release()));
        }
        void on_write(bool close, error_code ec, std::size_t bytes_transferred)
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}(close: {}, transferred: {})", __func__, close, bytes_transferred);

            boost::ignore_unused(bytes_transferred);

            if (ec) {
                CUTE_BEAST_LOG_ERROR(logger_, "{}: {}", __func__, ec.message());
                return;
            }

            if (close) {
                // This means we should close the connection, usually because
                // the response indicated the "Connection: close" semantic.
                return impl_->do_eof();
            }

            // Read another request
            do_read_header();
        }

    public:
        void send(http_generator&& gen)
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}({})", __func__, gen.result());
            impl_->send(std::move(gen));
        }
        const auto& url() const
        {
            return url_;
        }
    };

    // Detects SSL handshakes
    class ssl_detector : public std::enable_shared_from_this<ssl_detector>
    {
        beast::tcp_stream stream_;
        std::shared_ptr<router> router_;
        std::shared_ptr<ssl::context> ssl_ctx_;
        beast::flat_buffer buffer_ = beast::flat_buffer { session::start_buffer_size };
        CUTE_BEAST_LOGGER_T logger_ = CUTE_BEAST_MAKE_LOGGER("ssl_detector_{}", (void*)this);

    public:
        ssl_detector(tcp::socket&& socket, std::shared_ptr<router> router, std::shared_ptr<ssl::context> ssl_ctx)
            : stream_(std::move(socket))
            , router_(std::move(router))
            , ssl_ctx_(std::move(ssl_ctx))
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);
        }

        // Launch the detector
        void run()
        {
            CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

            // Set the timeout.
            beast::get_lowest_layer(stream_).expires_after(std::chrono::seconds(30));
            // Detect a TLS handshake
            async_detect_ssl(stream_, buffer_, beast::bind_front_handler(&ssl_detector::on_detect, shared_from_this()));
        }

        void on_detect(error_code ec, bool is_ssl_detected)
        {
            if (ec) {
                CUTE_BEAST_LOG_ERROR(logger_, "{}: {}", __func__, ec.message());
                return;
            }

            if (is_ssl_detected) {
                CUTE_BEAST_LOG_DEBUG(logger_, "{} ssl session detected", __func__);

                // Launch SSL session
                std::make_shared<session::http>(stream_.release_socket(), std::move(buffer_), *router_, std::move(ssl_ctx_))->run();
                return;
            }
            CUTE_BEAST_LOG_TRACE(logger_, "{} plain session detected", __func__);
            // Launch plain session
            std::make_shared<session::http>(stream_.release_socket(), std::move(buffer_), *router_)->run();
        }
    };

} // namespace session

//-------------------------------------------------------------- router impl
router::router(std::vector<route_ptr> rules)
    : http_rules_(std::move(rules))
{
    CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);
}
void router::add_rule(route_ptr ptr)
{
    std::lock_guard g { http_mutex_ };

    CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

    http_rules_.emplace_back(std::move(ptr));
}
/**
     * @one can add rule while router already serves handlers
     * @tparam ArgsT
     * @tparam RequestBodyT
     * @tparam Handler
     * @param pattern
     * @param handler
     * @param methods
     */
template <class ArgsT, class RequestBodyT, class Handler>
void router::add_rule(std::string_view pattern, Handler&& handler, std::initializer_list<http::verb>&& methods)
{
    add_rule(make_route_rule<ArgsT, RequestBodyT, Handler>(
        pattern, std::forward<decltype(handler)>(handler), std::forward<decltype(methods)>(methods)));
}
size_t router::remove_matched(string_view pattern)
{
    std::lock_guard g { http_mutex_ };
    CUTE_BEAST_LOG_TRACE(logger_, "{}({})", __func__, pattern.data());

    auto size_before = http_rules_.size();
    http_rules_.erase(
        std::remove_if(http_rules_.begin(), http_rules_.end(), [pattern](const auto& el) { return el->is_matching(pattern); }),
        http_rules_.end());
    return http_rules_.size() - size_before;
}
void router::add_websocket_rule(std::string path, std::function<void(std::shared_ptr<session::websocket>)> cb)
{
    std::lock_guard g { websocket_mutex_ };
    CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

    ws_rules_[path] = cb;
}
size_t router::remove_websocket_matched(std::string pattern)
{
    std::lock_guard g { websocket_mutex_ };
    CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

    auto size_before = ws_rules_.size();
    ws_rules_.erase(pattern);
    return ws_rules_.size() - size_before;
}
[[nodiscard]] const basic_http_route_rule* router::match(session::http& session) const
{
    std::shared_lock g { http_mutex_ };
    CUTE_BEAST_LOG_TRACE(logger_, "{}(session: {})", __func__, (void*)&session);

    for (const auto& rule : http_rules_) {
        auto match_result = rule->is_matching(session);
        if (match_result) {
            session.match_result_ = std::move(match_result);
            return rule.get();
        }
    }
    return {};
}
[[nodiscard]] std::function<void(std::shared_ptr<session::websocket>)> router::match(const session::websocket& session) const
{
    std::shared_lock g { websocket_mutex_ };
    CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

    if (session.req_) {
        auto it = ws_rules_.find(session.req_->target().to_string());
        if (it != ws_rules_.end()) {
            return it->second;
        }
    }
    return {};
}
//-------------------------------------------------------------- router impl end

template <class RequestT> struct http_route_rule : public basic_http_route_rule {
    using request_type = RequestT;
    using body_type = typename request_type::body_type;
    std::function<void(session::http&&)> handler;

public:
    http_route_rule(std::string_view pattern, std::uint64_t methods)
        : basic_http_route_rule(pattern, methods)
    {
    }
    [[nodiscard]] match_result is_matching(session::http& session) const override
    {
        CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

        auto parser = detail::parser_ptr<body_type>(session);
        auto& req = parser->get();
        auto method = req.method();

        if (!(methods_ & (1 << static_cast<std::underlying_type_t<http::verb>>(method)))) {
            return {};
        }
        auto url = boost::urls::parse_relative_ref(req.target());
        if (url.has_error()) {
            CUTE_BEAST_LOG_ERROR(
                logger_, "unable to parse request target field: {} cause of {} ", __func__, req.target(), url.error().message());

            return {};
        }
        detail::session_set_url(session, std::move(*url));
        return basic_http_route_rule::is_matching(session.url().encoded_path());
    }

    void on_header_done(session::http& session) const override
    {
        CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

        /* auto& parser =*/detail::morph_parser<body_type, beast::http::string_body>(session);
    }

    void handle_request(session::http&& session) const override
    {
        CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);

        handler(std::move(session));
    }
};

namespace util
{
    void send(session::http&& session, http_generator&& gen)
    {
        session.send(std::move(gen));
    }

} // namespace util

template <class ArgsT, class RequestBodyT, class Handler>
route_ptr make_route_rule(std::string_view pattern, Handler&& handler, std::initializer_list<http::verb>&& methods)
{
    using body_t = RequestBodyT;
    using request_t = typename http::request<body_t>;
    using regex_args_t = ArgsT;

    auto result = std::make_unique<http_route_rule<typename std::decay<request_t>::type>>(pattern, detail::make_methods_mask(methods));
    auto logger = result->logger();
    result->handler = [=](session::http&& session) {
        regex_args_t regex_args;
        auto& req = detail::get_request<session::http, body_t>(session);

        if constexpr (std::tuple_size_v < regex_args_t >> 0) {
            auto&& match_result = detail::session_match_result(session);

            if (match_result->size() < std::tuple_size_v<decltype(regex_args)>) {
                CUTE_BEAST_LOG_ERROR(
                    logger, "{} : {} < {}", "missing required URI args", match_result->size(), std::tuple_size_v<decltype(regex_args)>);

                return; //TODO: invalid match, captured less groups than args count
                //TODO: think how to add support for optional/default arguments also...
            }

            error_code ec;
            std::apply([&](auto&&... args) { detail::variadic_convert(1, *match_result, args..., ec); }, regex_args);
            if (ec) {
                CUTE_BEAST_LOG_ERROR(logger, "{}: {} ", "unable to convert some URI args", ec.message());
                util::send(std::move(session), util::make_bad_request(req, "unable to convert request args"));
                return;
            }
        }
        auto args = std::tuple_cat(std::tie(session, req), regex_args);
        CUTE_BEAST_LOG_TRACE(logger, "before applying handler");
        try {
            auto gen = http_generator(std::apply(handler, args));
            session.send(std::move(gen));
        } catch (const std::exception& e) {
            session.send(util::make_internal_server_error(req, e.what()));
        } catch (...) {
            session.send(util::make_internal_server_error(req));
        }

        CUTE_BEAST_LOG_TRACE(logger, "done applying handler");
    };

    return result;
}

// Accepts incoming connections and launches the sessions
template <class executor_t> class listener : public std::enable_shared_from_this<listener<executor_t>>
{
    executor_t& ioc_;
    tcp::endpoint endpoint_;
    tcp::acceptor acceptor_;
    std::shared_ptr<router> router_;
    std::shared_ptr<ssl::context> ssl_ctx_;
    session_mode mode_;
    CUTE_BEAST_LOGGER_T logger_ = CUTE_BEAST_MAKE_LOGGER("listener_{}", (void*)this);

public:
    listener(executor_t& ioc, tcp::endpoint endpoint, std::shared_ptr<router> router, session_mode mode = session_mode::http,
        std::shared_ptr<ssl::context> ssl_ctx = {})
        : ioc_(ioc)
        , acceptor_(net::make_strand(ioc))
        , endpoint_(std::move(endpoint))
        , router_(std::move(router))
        , mode_(mode)
        , ssl_ctx_(std::move(ssl_ctx))
    {
        CUTE_BEAST_LOG_INFO(logger_, "{}(ep: {}:{} , mode: {}, ssl_ctx: {}", __func__, endpoint_.address().to_string(),
            int(endpoint_.port()),
            mode == session_mode::http        ? "http"
                : mode == session_mode::https ? "https"
                                              : "https+http",
            !!ssl_ctx);
    }

    // Start accepting incoming connections
    [[nodiscard]] bool run(error_code& ec)
    {
        CUTE_BEAST_LOG_TRACE(logger_, "{}()", __func__);
        // Open the acceptor
        acceptor_.open(endpoint_.protocol(), ec);
        if (ec) {
            CUTE_BEAST_LOG_ERROR(logger_, "{}, {}: {}", __func__, "open", ec.message());
            return false;
        }

        // Allow address reuse
        acceptor_.set_option(net::socket_base::reuse_address(true), ec);
        if (ec) {
            CUTE_BEAST_LOG_ERROR(logger_, "{}, {}: {}", __func__, "reuse_address", ec.message());
            return false;
        }

        // Bind to the server address
        acceptor_.bind(endpoint_, ec);
        if (ec) {
            CUTE_BEAST_LOG_ERROR(logger_, "{}, {}: {}", __func__, "bind", ec.message());
            return false;
        }

        // Start listening for connections
        acceptor_.listen(net::socket_base::max_listen_connections, ec);
        if (ec) {
            CUTE_BEAST_LOG_ERROR(logger_, "{}, {}: {}", __func__, "listen", ec.message());
            return false;
        }
        CUTE_BEAST_LOG_INFO(logger_, "start accepting incoming connections ");

        do_accept();
        return true;
    }

private:
    void do_accept()
    {
        // The new connection gets its own strand
        acceptor_.async_accept(net::make_strand(ioc_), beast::bind_front_handler(&listener::on_accept, this->shared_from_this()));
    }

    virtual void on_accept(error_code ec, tcp::socket&& socket)
    {
        if (ec) {
            CUTE_BEAST_LOG_ERROR(logger_, "{}: {}", ec.message());
            do_accept();
            return;
        } else {
            auto client_endpoint = socket.remote_endpoint(ec);
            if (ec) {
                CUTE_BEAST_LOG_ERROR(logger_, "unable to get remote endpoint: {}", ec.message());
                do_accept();
                return;
            }
            CUTE_BEAST_LOG_DEBUG(logger_, "{}: {}:{}", __func__, client_endpoint.address().to_string(), int(client_endpoint.port()));

            switch (mode_) {
            case session_mode::both_http_https:
                assert(ssl_ctx_);
                std::make_shared<session::ssl_detector>(std::move(socket), router_, ssl_ctx_)->run();
                break;
            case session_mode::https:
                assert(ssl_ctx_);
                std::make_shared<session::http>(std::move(socket), beast::flat_buffer { session::start_buffer_size }, *router_, ssl_ctx_)
                    ->run();
                break;
            case session_mode::http:
                std::make_shared<session::http>(std::move(socket), beast::flat_buffer { session::start_buffer_size }, *router_)->run();
                break;
            }
        }
        // Accept another connection
        do_accept();
    }
};

} // namespace cute_beast

#endif //CUTE_BEAST_SESSION_HPP
