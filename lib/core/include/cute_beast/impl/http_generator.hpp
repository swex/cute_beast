#ifndef CUTE_BEAST_HTTP_GENERATOR_HPP
#define CUTE_BEAST_HTTP_GENERATOR_HPP

#include "detail.hpp"
#include <iostream>

namespace cute_beast
{

class http_generator
{
public:
    class const_buffers_type
    {
        net::const_buffer const* begin_ = nullptr;
        net::const_buffer const* end_ = nullptr;

    public:
        using value_type = net::const_buffer;

        const_buffers_type() = default;
        const_buffers_type(const_buffers_type const&) = default;
        const_buffers_type& operator=(const_buffers_type const&) = default;

        const_buffers_type(net::const_buffer const* p, std::size_t n)
            : begin_(p)
            , end_(p + n)
        {
        }

        value_type const* begin() const noexcept
        {
            return begin_;
        }

        value_type const* end() const noexcept
        {
            return end_;
        }
    };

    template <bool isRequest, class Body, class Fields>
    http_generator(http::message<isRequest, Body, Fields>&& m)
        : impl_(std::make_shared<generator_impl<isRequest, Body, Fields>>(std::move(m)))
    {
    }

    const_buffers_type prepare(error_code& ec)
    {
        return impl_->prepare(ec);
    }

    void consume(std::size_t n)
    {
        impl_->consume(n);
    }

    bool keep_alive() const noexcept
    {
        return impl_->keep_alive();
    }
    bool need_eof() const noexcept
    {
        return impl_->need_eof();
    }

    http::status result() const noexcept
    {
        return impl_->result();
    }

private:
    struct generator {
        virtual ~generator() = default;

        virtual const_buffers_type prepare(error_code& ec) = 0;

        virtual void consume(std::size_t n) = 0;

        virtual bool keep_alive() const noexcept = 0;

        virtual bool need_eof() const noexcept = 0;

        virtual http::status result() const noexcept = 0;
    };

    template <bool isRequest, class Body, class Fields> struct generator_impl : generator {
        static std::size_t constexpr max_buf = 16;

        http::message<isRequest, Body, Fields> m_;
        http::serializer<isRequest, Body, Fields> sr_;
        net::const_buffer buf_[max_buf];

        explicit generator_impl(http::message<isRequest, Body, Fields>&& m)
            : m_(std::move(m))
            , sr_(m_)
        {
        }

        struct visit {
            generator_impl* this_;
            const_buffers_type& cb_;

            template <class ConstBufferSequence> void operator()(error_code&, ConstBufferSequence const& buffers)
            {
                auto it = net::buffer_sequence_begin(buffers);
                auto const end = net::buffer_sequence_end(buffers);
                std::size_t n = 0;
                while (it != end && n < max_buf)
                    this_->buf_[n++] = *it++;
                cb_ = { this_->buf_, n };
            }
        };

        const_buffers_type prepare(error_code& ec) override
        {
            if (sr_.is_done())
                return {};
            const_buffers_type cb;
            sr_.next(ec, visit { this, cb });
            return cb;
        }

        void consume(std::size_t n) override
        {
            sr_.consume(n);
        }

        bool keep_alive() const noexcept override
        {
            return m_.keep_alive();
        }
        bool need_eof() const noexcept override
        {
            return m_.need_eof();
        }
        http::status result() const noexcept override
        {
            return m_.result();
        }
    };

    std::shared_ptr<generator> impl_;
};

namespace detail
{

    template <class T, class = void> struct is_buffers_generator : std::false_type {
    };

    template <class T>
    struct is_buffers_generator<T,
        beast::detail::void_t<decltype(typename T::const_buffers_type(std::declval<T&>().prepare(std::declval<error_code&>())),
            std::declval<T&>().consume(std::size_t {}))>> : std::true_type {
    };

    template <class Handler, class Stream, class Generator>
    class write_op : public beast::async_base<Handler, beast::executor_type<Stream>>, public net::coroutine
    {
        Stream& s_;
        Generator gen_;
        std::size_t bytes_transferred_ = 0;

    public:
        template <class Handler_>
        write_op(Handler_&& h, Stream& s, Generator&& gen)
            : beast::async_base<Handler, beast::executor_type<Stream>>(std::forward<Handler_>(h), s.get_executor())
            , s_(s)
            , gen_(gen)
        {
            (*this)();
        }

        void operator()(error_code ec = {}, std::size_t bytes_transferred = 0)
        {
            BOOST_ASIO_CORO_REENTER(*this)
            {

                for (;;) {
                    BOOST_ASIO_CORO_YIELD
                    {
                        BOOST_ASIO_HANDLER_LOCATION((__FILE__, __LINE__, "cute_beast::async_write"));
                        auto cb = gen_.prepare(ec);
                        if (ec) {
                            goto upcall;
                        }
                        if (beast::buffer_bytes(cb) == 0) {
                            goto upcall;
                        }
                        s_.async_write_some(cb, std::move(*this));
                    }
                    gen_.consume(bytes_transferred);
                    if (ec)
                        goto upcall;
                    bytes_transferred_ += bytes_transferred;
                }
            upcall:
                this->complete_now(ec, bytes_transferred_);
            }
        }
    };

    struct run_write_op {
        template <class WriteHandler, class Stream, class Generator> void operator()(WriteHandler&& h, Stream* s, Generator&& gen)
        {
            // If you get an error on the following line it means
            // that your handler does not meet the documented type
            // requirements for the handler.

            static_assert(
                beast::detail::is_invocable<WriteHandler, void(error_code, std::size_t)>::value, "WriteHandler type requirements not met");

            write_op<typename std::decay<WriteHandler>::type, Stream, Generator>(
                std::forward<WriteHandler>(h), *s, std::forward<Generator>(gen));
        }
    };
} // namespace detail

template <class AsyncWriteStream, class BufferGenerator,
    BOOST_BEAST_ASYNC_TPARAM2 WriteHandler = net::default_completion_token_t<beast::executor_type<AsyncWriteStream>>>
BOOST_BEAST_ASYNC_RESULT2(WriteHandler)
async_write(
    AsyncWriteStream& stream, BufferGenerator&& msg,
    WriteHandler&& handler = net::default_completion_token_t<beast::executor_type<AsyncWriteStream>> {}
#ifndef BOOST_BEAST_DOXYGEN

#endif
)
{
    static_assert(detail::is_buffers_generator<BufferGenerator>::value, "");
    static_assert(beast::is_async_write_stream<AsyncWriteStream>::value, "AsyncWriteStream type requirements not met");
    return net::async_initiate<WriteHandler, void(error_code, std::size_t)>(
        detail::run_write_op {}, handler, &stream, std::forward<BufferGenerator>(msg));
}
} // namespace cute_beast

#endif //CUTE_BEAST_HTTP_GENERATOR_HPP
