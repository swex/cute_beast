//
// Created by swex on 1/19/21.
//

#ifndef QTBEASTSERVER_QTBEASTUTIL_HPP
#define QTBEASTSERVER_QTBEASTUTIL_HPP

#include <QCoreApplication>
#include <QIODevice>
#include <QPointer>
#include <algorithm>
#include <boost/asio/execution_context.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/noncopyable.hpp>
#include <cassert>
#include <memory>

namespace QtBeast {

namespace net = boost::asio;
namespace beast = boost::beast;
namespace http = beast::http;
using boost::beast::error_code;

struct ExecutionContext : net::execution_context, boost::noncopyable {
    explicit ExecutionContext(QObject* target = qApp);

    ~ExecutionContext();

    template <class F> void post(F f);

    static ExecutionContext& singleton();

    struct filter : QObject {
        filter();

        auto eventFilter(QObject*, QEvent* event) -> bool override;
    };

private:
    QObject* target_;
    filter filter_;
};

struct Executor {
    explicit Executor(QObject* contextGuard, ExecutionContext& context = ExecutionContext::singleton()) noexcept;

    ExecutionContext& query(net::execution::context_t) const noexcept;

    static constexpr net::execution::blocking_t query(net::execution::blocking_t) noexcept
    {
        return net::execution::blocking.never;
    }

    static constexpr net::execution::relationship_t query(net::execution::relationship_t) noexcept
    {
        return net::execution::relationship.fork;
    }

    static constexpr net::execution::outstanding_work_t query(net::execution::outstanding_work_t) noexcept
    {
        return net::execution::outstanding_work.tracked;
    }

    template <typename OtherAllocator> static constexpr auto query(net::execution::allocator_t<OtherAllocator>) noexcept
    {
        return std::allocator<void>();
    }

    static constexpr auto query(net::execution::allocator_t<void>) noexcept
    {
        return std::allocator<void>();
    }

    template <class F> void execute(F f) const
    {
        if (contextGuard_) {
            context_->post([guard = contextGuard_, f = std::move(f)]() mutable {
                if (guard) {
                    f();
                }
            });
        }
    }

    bool operator==(Executor const& other) const noexcept;
    bool operator!=(Executor const& other) const noexcept;

private:
    ExecutionContext* context_;
    QPointer<QObject> contextGuard_;
};
class EventBase : public QEvent {
public:
    EventBase();

    virtual void invoke() = 0;

    static QEvent::Type generated_type();
};

template <class F> struct Event : EventBase {
    Event(F f)
        : f_(std::move(f))
    {
    }

    void invoke() override
    {
        f_();
    }

private:
    F f_;
};

// impl

template <class F> void ExecutionContext::post(F f)
{
    // c++20 auto template deduction
    auto event = new Event(std::move(f));
    QCoreApplication::postEvent(target_, event);
}

template <class File = std::shared_ptr<QIODevice>> struct QIODeviceFileBody {

    // Algorithm for storing buffers when parsing.
    class reader;

    // Algorithm for retrieving buffers when serializing.
    class writer;

    // The type of the @ref message::body member.
    class value_type;

    /** Returns the size of the body

        @param body The file body to use
    */
    static std::uint64_t size(value_type const& body);
};

template <class File> class QIODeviceFileBody<File>::value_type {
    // This body container holds a handle to the file
    // when it is open, and also caches the size when set.

    friend class reader;
    friend class writer;
    friend struct QIODeviceFileBody;

    // This represents the open file
    std::shared_ptr<File> file_;

    // The cached file size
    std::uint64_t file_size_ = 0;

public:
    /** Destructor.

        If the file is open, it is closed first.
    */
    ~value_type() = default;

    /// Constructor
    value_type() = default;

    /// Constructor
    value_type(value_type&& other) = default;

    /// Move assignment
    value_type& operator=(value_type&& other) = default;

    /// Return the file
    File& file()
    {
        return file_;
    }

    /// Returns `true` if the file is open
    bool is_open() const
    {
        return file_->isOpen()();
    }

    /// Returns the size of the file if open
    std::uint64_t size() const
    {
        return file_size_;
    }

    /// Close the file if open
    void close();

    /** Open a file at the given path with the specified mode

        @param path The utf-8 encoded path to the file

        @param mode The file mode to use

        @param ec Set to the error, if any occurred
    */
    void open(QString path, QIODevice::OpenMode, error_code& ec);

    /** Set the open file

        This function is used to set the open file. Any previously
        set file will be closed.

        @param file The file to set. The file must be open or else
        an error occurs

        @param ec Set to the error, if any occurred
    */
    void reset(File&& file, error_code& ec);
};

template <class File> void QIODeviceFileBody<File>::value_type::close()
{
    file_.close();
}

template <class File> void QIODeviceFileBody<File>::value_type::open(QString path, QIODevice::OpenMode mode, error_code& ec)
{
    // Open the file
    file_ = std::make_shared<File>(path);
    auto opened = file_->open(mode);
    if (!opened) {
        ec = net::error::not_found;
        return;
    }

    // Cache the size
    file_size_ = file_->size();
    ec = {};
}

template <class File> void QIODeviceFileBody<File>::value_type::reset(File&& file, error_code& ec)
{
    // First close the file if open
    if (!file_) {
        return;
    }
    if (file_->isOpen()) {
        file_->close();
    }

    // Take ownership of the new file
    file_ = std::move(file);

    // Cache the size
    file_size_ = file_->size();
    ec = {};
};

// This is called from message::payload_size
template <class File> std::uint64_t QIODeviceFileBody<File>::size(value_type const& body)
{
    // Forward the call to the body
    return body.size();
}

//]

//[example_http_file_body_3

/** Algorithm for retrieving buffers when serializing.

    Objects of this type are created during serialization
    to extract the buffers representing the body.
*/
template <class File> class QIODeviceFileBody<File>::writer {
    value_type& body_; // The body we are reading from
    std::uint64_t remain_; // The number of unread bytes
    QByteArray buf_; // Buffer for reading

public:
    // The type of buffer sequence returned by `get`.
    //
    using const_buffers_type = net::const_buffer;

    // Constructor.
    //
    // `h` holds the headers of the message we are
    // serializing, while `b` holds the body.
    //
    // Note that the message is passed by non-const reference.
    // This is intentional, because reading from the file
    // changes its "current position" which counts makes the
    // operation logically not-const (although it is bitwise
    // const).
    //
    // The BodyWriter concept allows the writer to choose
    // whether to take the message by const reference or
    // non-const reference. Depending on the choice, a
    // serializer constructed using that body type will
    // require the same const or non-const reference to
    // construct.
    //
    // Readers which accept const messages usually allow
    // the same body to be serialized by multiple threads
    // concurrently, while readers accepting non-const
    // messages may only be serialized by one thread at
    // a time.
    //
    template <bool isRequest, class Fields> writer(http::header<isRequest, Fields>& h, value_type& b);

    // Initializer
    //
    // This is called before the body is serialized and
    // gives the writer a chance to do something that might
    // need to return an error code.
    //
    void init(error_code& ec);

    // This function is called zero or more times to
    // retrieve buffers. A return value of `boost::none`
    // means there are no more buffers. Otherwise,
    // the contained pair will have the next buffer
    // to serialize, and a `bool` indicating whether
    // or not there may be additional buffers.
    boost::optional<std::pair<const_buffers_type, bool>> get(error_code& ec);
};

//]

//[example_http_file_body_4

// Here we just stash a reference to the path for later.
// Rather than dealing with messy constructor exceptions,
// we save the things that might fail for the call to `init`.
//
template <class File>
template <bool isRequest, class Fields>
QIODeviceFileBody<File>::writer::writer(http::header<isRequest, Fields>& h, value_type& b)
    : body_(b)
{
    boost::ignore_unused(h);

    // The file must already be open
    BOOST_ASSERT(body_.file_->isOpen());

    // Get the size of the file
    remain_ = body_.file_size_;
}

// Initializer
template <class File> void QIODeviceFileBody<File>::writer::init(error_code& ec)
{
    // The error_code specification requires that we
    // either set the error to some value, or set it
    // to indicate no error.
    //
    // We don't do anything fancy so set "no error"
    buf_.reserve(std::min(remain_, uint64_t(1024 * 1024 * 10)));
    ec = {};
}

// This function is called repeatedly by the serializer to
// retrieve the buffers representing the body. Our strategy
// is to read into our buffer and return it until we have
// read through the whole file.
//
template <class File> auto QIODeviceFileBody<File>::writer::get(error_code& ec) -> boost::optional<std::pair<const_buffers_type, bool>>
{
    // Calculate the smaller of our buffer size,
    // or the amount of unread data in the file.
    auto const amount = remain_ > buf_.capacity() ? buf_.capacity() : static_cast<std::size_t>(remain_);

    // Handle the case where the file is zero length
    if (amount == 0) {
        // Modify the error code to indicate success
        // This is required by the error_code specification.
        //
        // NOTE We use the existing category instead of calling
        //      into the library to get the generic category because
        //      that saves us a possibly expensive atomic operation.
        //
        ec = {};
        return boost::none;
    }

    // Now read the next buffer
    auto const nread = body_.file_->read(buf_.data(), amount);
    if (nread <= 0) {
        ec = boost::beast::http::error::short_read;
        return boost::none;
    }

    // Make sure there is forward progress
    BOOST_ASSERT(nread != 0);
    BOOST_ASSERT(nread <= remain_);

    // Update the amount remaining based on what we got
    remain_ -= nread;

    // Return the buffer to the caller.
    //
    // The second element of the pair indicates whether or
    // not there is more data. As long as there is some
    // unread bytes, there will be more data. Otherwise,
    // we set this bool to `false` so we will not be called
    // again.
    //
    ec = {};
    return { {
        const_buffers_type { buf_.data(), std::size_t(nread) }, // buffer to return.
        remain_ > 0 // `true` if there are more buffers.
    } };
}

//]

//[example_http_file_body_5

/** Algorithm for storing buffers when parsing.

    Objects of this type are created during parsing
    to store incoming buffers representing the body.
*/
template <class File> class QIODeviceFileBody<File>::reader {
    value_type& body_; // The body we are writing to

public:
    // Constructor.
    //
    // This is called after the header is parsed and
    // indicates that a non-zero sized body may be present.
    // `h` holds the received message headers.
    // `b` is an instance of `QIODeviceFileBody`.
    //
    template <bool isRequest, class Fields> explicit reader(http::header<isRequest, Fields>& h, value_type& b);

    // Initializer
    //
    // This is called before the body is parsed and
    // gives the reader a chance to do something that might
    // need to return an error code. It informs us of
    // the payload size (`content_length`) which we can
    // optionally use for optimization.
    //
    void init(boost::optional<std::uint64_t> const&, error_code& ec);

    // This function is called one or more times to store
    // buffer sequences corresponding to the incoming body.
    //
    template <class ConstBufferSequence> std::size_t put(ConstBufferSequence const& buffers, error_code& ec);

    // This function is called when writing is complete.
    // It is an opportunity to perform any final actions
    // which might fail, in order to return an error code.
    // Operations that might fail should not be attempted in
    // destructors, since an exception thrown from there
    // would terminate the program.
    //
    void finish(error_code& ec);
};

//]

//[example_http_file_body_6

// We don't do much in the reader constructor since the
// file is already open.
//
template <class File>
template <bool isRequest, class Fields>
QIODeviceFileBody<File>::reader::reader(http::header<isRequest, Fields>& h, value_type& body)
    : body_(body)
{
    boost::ignore_unused(h);
}

template <class File> void QIODeviceFileBody<File>::reader::init(boost::optional<std::uint64_t> const& content_length, error_code& ec)
{
    // The file must already be open for writing
    BOOST_ASSERT(body_.file_.is_open());

    // We don't do anything with this but a sophisticated
    // application might check available space on the device
    // to see if there is enough room to store the body.
    boost::ignore_unused(content_length);

    // The error_code specification requires that we
    // either set the error to some value, or set it
    // to indicate no error.
    //
    // We don't do anything fancy so set "no error"
    ec = {};
}

// This will get called one or more times with body buffers
//
template <class File> template <class ConstBufferSequence> std::size_t QIODeviceFileBody<File>::reader::put(ConstBufferSequence const& buffers, error_code& ec)
{
    // This function must return the total number of
    // bytes transferred from the input buffers.
    std::size_t nwritten = 0;

    // Loop over all the buffers in the sequence,
    // and write each one to the file.
    for (auto it = net::buffer_sequence_begin(buffers); it != net::buffer_sequence_end(buffers); ++it) {
        // Write this buffer to the file
        net::const_buffer buffer = *it;
        nwritten += body_.file_.write(buffer.data(), buffer.size(), ec);
        if (ec)
            return nwritten;
    }

    // Indicate success
    // This is required by the error_code specification
    ec = {};

    return nwritten;
}

// Called after writing is done when there's no error.
template <class File> void QIODeviceFileBody<File>::reader::finish(error_code& ec)
{
    // This has to be cleared before returning, to
    // indicate no error. The specification requires it.
    ec = {};
}

} // namespace QtBeast

#endif //QTBEASTSERVER_QTBEASTUTIL_HPP
