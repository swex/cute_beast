#pragma once

#include "QtBeastServer_global.h"
#include <QCoreApplication>
#include <QHostAddress>
#include <QObject>
#include <QPointer>
#include <boost/asio/ssl.hpp>
#include <boost/beast/http/message.hpp>
#include <memory>

namespace QtBeast {

namespace http = boost::beast::http;

enum class ServerListenMode {
    http,
    https,
    both_http_https,
};
struct ServerPrivate;
class QTBEASTSERVER_EXPORT Server : public QObject {
    Q_OBJECT

public:
    explicit Server(QObject* parent = nullptr);
    ~Server();
    bool listen(const QHostAddress& address = QHostAddress::Any, quint16 port = 8080, ServerListenMode mode = ServerListenMode::http, std::shared_ptr<boost::asio::ssl::context> ssl_ctx = {});
    bool isListening() const;

public:
    //    void registerHttpTarget(QString target_regex, QVariant handler);
    //    void registerHttpTargetForStringBody(QString target_regex, HandlerT handler);
public slots:
    void registerStaticDirectoryServer(QString directoryPath, QString target_regex = R"__(^\/(.+)$)__");
    size_t deregisterPath(QString target_regex);

private:
    std::unique_ptr<ServerPrivate> q_ptr;
    Q_DECLARE_PRIVATE(Server)
    Q_DISABLE_COPY(Server)
};

} // namespace QtBeast
